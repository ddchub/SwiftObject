//
//  AppDelegate.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/12/3.
//  Copyright © 2018 wangws1990. All rights reserved.
//
import UIKit
@UIApplicationMain

class AppDelegate: UIResponder,UIApplicationDelegate {
    var window: UIWindow?
    var rotation : UIInterfaceOrientationMask = .portrait
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        BaseRouter.startRouter()
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.backgroundColor = UIColor.systemBackground
        let vc = GKLaunchController {
            self.window?.rootViewController = GKTabBarController()
            self.window?.overrideUserInterfaceStyle = moya.interfaceStyle
        }
        self.window?.rootViewController = vc
        self.window?.overrideUserInterfaceStyle = moya.interfaceStyle
        self.window?.makeKeyAndVisible()
        return true
    }
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return  self.rotation == .portrait ? .portrait : .landscapeRight
    }
}
