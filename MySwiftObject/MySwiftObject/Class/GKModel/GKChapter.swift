//
//  GKChapter.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/10.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKChapter: HandyJSON {
    var bookId      :String = "";
    var chapterId   :String = "";
    var chapterCover:String = "";
    var link        :String = "";
    var time        :String = "";
    var title       :String = "";
    
    var isVip       :Bool   = false;
    
    var order       :Int = 0;
    var chapterIndex:Int = 0;
    var partsize    :Int = 0;
    var totalpage   :Int = 0;
    var currency    :Int = 0;
    
    required init() {}
    func mapping(mapper: HelpingMapper) {
        mapper <<< self.chapterId <-- ["chapterId","id"]
    }
    var content : GKContent?{
        let json = ApiCache.values(key: self.chapterId).json
        if json.count == 0 {
            return nil
        }
        guard let info =  GKContent.deserialize(from: json.rawString()) else { return nil}
        info.chapterId = self.chapterId
        info.title = self.title
        return info
    }
}
class GKChapterInfo: HandyJSON {
    var _id:String = "";
    var source:String = "";
    var updated:String = "";
    var book:String = "";
    var starting:String = "";
    var link:String = "";
    var host:String = "";
    var name:String = "";
    
    var chapters:[GKChapter] = [];
    required init() {}
}
