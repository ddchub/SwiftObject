//
//  GKBookSet.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/12.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
private let eyeColor    :String = "268451"
private let nightColor  :String = "181818"
private let normalColor :String = "D9C8AC"

private let fontColor1  :String = "252525"
private let fontColor2  :String = "333333"
private let fontColor3  :String = "888888"
private let fontColor4  :String = "DFD6D6"
private let fontColor5  :String = "CFDFD7"
private let fontColor6  :String = "9DA3AC"
public class GKBookSet: HandyJSON {
    var fontName    :String               = "PingFang-SC-Regular"
    var fontSize    : Float               = 22
    var lineSpacing :Float                = 2
    var firstLineHeadIndent    :Float     = 36
    var paragraphSpacingBefore :Float     = 4
    var paragraphSpacing       :Float     = 4
    var brightness             :Float     = 0
    
    var night      :Bool                  = false
    var landscape  :Bool                  = false
    var traditiona :Bool                  = false
    
    var theme      :GKBookTheme           =  GKBookTheme.defaultlTheme()
    var browse     :GKNovelBrowse         = .defaults
    required public init() {}
}
//MARK:阅读皮肤
public class GKBookTheme :HandyJSON{
    var fontColor   :String               = fontColor1 //字体原色
    var themeColor  :String               = normalColor//阅读背景
    var placeholder :String?              = nil//图片地址或图片名称
    var title       :String?              = nil//主题名称
    convenience init(themeColor :String = normalColor,fontColor :String = fontColor1,placeholder :String? = nil,title :String? = nil) {
        self.init()
        self.fontColor = fontColor
        self.themeColor = themeColor
        self.placeholder = placeholder
        self.title = title
    }
    static func defaultlTheme() ->GKBookTheme{
        return GKBookTheme(themeColor: normalColor, fontColor: fontColor1,title: "旧纸质")
    }
    static func nightTheme() ->GKBookTheme{
        return GKBookTheme(themeColor: nightColor, fontColor: fontColor6,title:"夜间")
    }
    static func projectEyeTheme() ->GKBookTheme{
        return GKBookTheme(themeColor: eyeColor, fontColor: fontColor5,title:"护眼绿")
    }
    static func themeColorDatas() ->[GKBookTheme]{
        let item1 = defaultlTheme()
        let item2 = GKBookTheme(themeColor: "E6E0C4", fontColor: fontColor1, title: "桔黄色")
        let item3 = GKBookTheme(themeColor: "101010", fontColor: fontColor6, title: "纯黑色")

        let item4 = GKBookTheme(themeColor: "CEB89B", fontColor: fontColor1, title: "实木色")
        let item5 = GKBookTheme(themeColor: "BFB2C5", fontColor: fontColor1, title: "淡紫色")
        let item6 = GKBookTheme(themeColor: "D2D3D7", fontColor: fontColor1, title: "浅灰色")
        let item7 = GKBookTheme(themeColor: "BBCBD4", fontColor: fontColor1, title: "灰蓝色")
        let item8 = GKBookTheme(themeColor: "6A6A6A", fontColor: fontColor4, title: "暗淡灰")
        let item9 = GKBookTheme(themeColor: "E1D3CB", fontColor: fontColor1, title: "蜜色")
        let item0 = projectEyeTheme()
        return [item1,item2,item3,item4,item5,item6,item7,item8,item9,item0]
    }
    static func themeCoverDatas() ->[GKBookTheme]{
        let item0 = GKBookTheme(themeColor: "",
                                fontColor: fontColor2,
                                placeholder: GKNovelTheme.defaults.rawValue,
                                title: "旧纸革")
        return [item0]
    }
    required public init() {}
}
