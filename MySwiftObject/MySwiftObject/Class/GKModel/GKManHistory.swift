//
//  GKManHistory.swift
//  MySwiftObject
//
//  Created by anscen on 2022/1/18.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit

class GKManHistory: HandyJSON {
    
    var comicId    :String?       = ""
    var insertTime :TimeInterval  = 0
    var updateTime :TimeInterval  = 0
    var chapter    :NSInteger     = 0
    var pageIndex  :NSInteger     = 0
    
    var info       :GKManItem?        = nil
    var chapters   :[GKManChapter]?   = nil
    required init() {}
}
