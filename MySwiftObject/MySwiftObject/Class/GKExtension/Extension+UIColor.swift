//
//  Extension+UIColor.swift
//  MySwiftObject
//
//  Created by anscen on 2022/1/14.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit
extension UIColor{
    static func appColor() ->UIColor{
        return UIColor(hex:"3991F0")
    }
    static func appx000000() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"9DA3AC")
            } else {
                return UIColor(hex:"000000")
            }
        }
        return color
    }
    static func appx333333() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"636C78")
            } else {
                return UIColor(hex:"33383E")
            }
        }
        return color
    }
    static func appx666666() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"636C78")
            } else {
                return UIColor(hex:"636C78")
            }
        }
        return color
    }
    static func appx999999() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"636C78")
            } else {
                return UIColor(hex:"9DA3AC")
            }
        }
        return color
    }
    static func appxdddddd() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"33383E")
            } else {
                return UIColor(hex:"dddddd")
            }
        }
        return color
    }
    static func appxe5e5e5() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"33383E")
            } else {
                return UIColor(hex:"e5e5e5")
            }
        }
        return color
    }
    static func appxd0d0d0() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"33383E")
            } else {
                return UIColor(hex:"d0d0d0")
            }
        }
        return color
    }
    static func appxffffff() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            //debugPrint(trainCollection.userInterfaceStyle.rawValue)
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"101010")
            } else {
                return UIColor(hex:"ffffff")
            }
        }
        return color
    }
    static func appxf1f2f4() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"33383E")
            } else {
                return UIColor(hex:"F1F2F4")
            }
        }
        return color
    }
    static func appxAlapha() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            return UIColor(hex: "000000").alpha(0.3)
        }
        return color
    }
    static func appxf8f8f8() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"181818")
            } else {
                return UIColor(hex:"f8f8f8")
            }
        }
        return color
    }
    static func appxf4f4f4() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"333333")
            } else {
                return UIColor(hex:"f4f4f4")
            }
        }
        return color
    }
    static func appAlertColor() ->UIColor{
        let color = UIColor { (trainCollection) -> UIColor in
            if trainCollection.userInterfaceStyle == .dark {
                return UIColor(hex:"181818")
            } else {
                return UIColor(hex:"ffffff")
            }
        }
        return color
    }
}
extension UIView{
    public func topViewController()-> UIViewController?{
        var nextView = self.next
        while (nextView != nil) {
            if let respond = nextView, respond is UIViewController{
                return respond as? UIViewController
            }
            nextView = nextView?.next
        }
        return nil
    }
}
