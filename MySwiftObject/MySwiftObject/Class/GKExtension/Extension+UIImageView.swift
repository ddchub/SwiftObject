//
//  GKLoad.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/25.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

extension UIImageView{
    public func setGkImageWithURL(url:String){
        self.setGkImageWithURL(url: url, placeholder: UIImage.placeholder())
    }
    public func setWallImageWithURL(url:String){
        self.sd_setImage(with: URL(string: url), placeholderImage: UIImage.placeholder(), options:SDWebImageOptions(rawValue: 0)) { image, error, type, url in

        }
    }
    public func setGkImageWithURL(url:String,placeholder:UIImage){
        self.setGkImageWithURL(url: url, placeholder: placeholder, unencode: true) { image in
            
        };
    }
    public func setGkImageWithURL(url:String,sucesss:@escaping (_ image : UIImage?) ->()){
        self.setGkImageWithURL(url: url, placeholder: UIImage.placeholder(), unencode: true,sucesss: sucesss)
    }
    public func setGkImageWithURL(url:String,placeholder:UIImage,unencode:Bool,sucesss:@escaping (_ image : UIImage?) ->()?){
        var str : String = "";
        if url.hasPrefix("/agent/"){
            str = url.replacingOccurrences(of: "/agent/", with: "");
        }
        str = unencode ? str.removingPercentEncoding! : str;
        //kf 有严重的性能问题
        //self.kf.setImage(with: URL(string: str),placeholder: placeholder)
//        self.sd_setImage(with: URL(string: str), placeholderImage: placeholder)
        self.sd_setImage(with: URL(string: str), placeholderImage: placeholder, options:SDWebImageOptions(rawValue: 0)) { image, error, type, url in
            sucesss(image)
        }
    }
//    public func downGkImageWithURL(url:String,placeholder:UIImage,unencode:Bool,completion:@escaping((_ image:UIImage,_ success:Bool) -> Void)){
//        var str : String!;
//        if url.hasPrefix("/agent/"){
//            str = url.replacingOccurrences(of: "/agent/", with: "");
//        }
//        str = unencode ? str.removingPercentEncoding : str;
//        self.kf.setImage(with: URL.init(string: str), placeholder: placeholder, options: nil, progressBlock: nil) { (Result<RetrieveImageResult, KingfisherError>) in
//            
//        }
//        self.sd_setImage(with: URL.init(string: str), placeholderImage: placeholder, options: .cacheMemoryOnly) { (image, error, type, url) in
//            if error == nil{
//                completion(image!,true);
//            }else{
//                completion(image!,false);
//            }
//        }
//    }
    
}
extension GKAnimatedImageView{
    //MARK:GIF图片赋值
    public func setGIFImageWithURL(url:String,placeholder:UIImage = UIImage.placeholder()){
        self.setGIFImageWithURL(url: url, placeholder: placeholder) { (image) in
            
        }
    }
    public func setGIFImageWithURL(url:String,placeholder:UIImage = UIImage.placeholder(),sucesss:@escaping (_ image : UIImage?) ->()){
        self.sd_setImage(with: URL(string: url), placeholderImage: placeholder, options: SDWebImageOptions(rawValue: 0), progress: nil) { (image, error, type, url) in
            DispatchQueue.main.async {
                sucesss(image)
            }
        }
    }
    //MARK:GIF图片下载
    public func downloadGif(url :String,sucesss:@escaping (_ image : UIImage?,_ url :String) ->()){
        self.sd_setImage(with: URL(string: url), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), progress: nil) { (image, error, type, urlString) in
            DispatchQueue.main.async {
                if (error != nil){
                    sucesss(nil,url)
                }else{
                    sucesss(image,url)
                }
            }
        }
    }
}
