//
//  GKEnum.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2021/11/3.
//  Copyright © 2021 wangws1990. All rights reserved.
//

import UIKit

public enum BThemeState :String,HandyJSONEnum{
    case blue  = "经典蓝"
    case red   = "活力红"
    case brown = "大地棕"
    case black = "炫酷黑"
    case orange  = "新鲜橙"
    case green   = "健康绿"
    case purple  = "高贵紫"
    case pink    = "可爱粉"
    
    case cyan    = "自然青"
    case yellow  = "魅力黄"
    case defaults = "默认"
}
public enum GKUserState : Int,HandyJSONEnum {
    case boy
    case girl
}
public enum GKLookState : String,HandyJSONEnum {
    case grid = "网格"
    case list = "列表"
}
public enum GKDartState : Int,HandyJSONEnum {
    case system = 0
    case light  = 1
    case dart   = 2
}
public enum GKNovelTheme : String,HandyJSONEnum{
    case defaults = "icon_default"
    case green    = "icon_read_green"
    case caffee   = "icon_read_coffee"
    case pink     = "icon_read_fenweak"
    case fen      = "icon_read_fen"
    case zi       = "icon_read_zi"
    case yellow   = "icon_read_yellow"
    case phone    = "icon_phone"
    case hite     = "icon_hite"
}
public enum GKNovelBrowse : String,HandyJSONEnum{
    case defaults = "默认"//默认
    case pageCurl = "仿真"//翻页
    case nones    = "无动画"//无动画
    case scroll   = "上下"//上下滑动
    case joined   = "连体"//左右连体
}
public enum GKTabType :String,HandyJSONEnum{
    case mulu  = "icon_read_mu"
    case day   = "icon_read_day"
    case night = "icon_read_night"
    case set   = "icon_read_set"
}
public enum GKManTabType :String,HandyJSONEnum{
    case mulu      = "icon_read_mulu"
    case port      = "icon_port"
    case right     = "icon_left"
    case britess   = "icon_read_britess"
}
public struct GKLoadOptions :OptionSet {
    public init(rawValue: Int) {
        self.rawValue = rawValue
    }
    public var rawValue: Int
    public static var none     : GKLoadOptions{return GKLoadOptions(rawValue: 1)}
    public static var net      : GKLoadOptions{return GKLoadOptions(rawValue: none.rawValue<<1)};
    public static var base     : GKLoadOptions{return GKLoadOptions(rawValue: none.rawValue<<2)};
    public static var defaults : GKLoadOptions{return GKLoadOptions(rawValue: net.rawValue|base.rawValue)}
}
public enum GKHomeMenu :String{
    case top      = "置顶本书"
    case bot      = "取消置顶"
    case mulu     = "目录书摘"
    case comment  = "查看书评"
    case down     = "下载本书"
    case share    = "分享本书"
    case delete   = "清除缓存"
    case deleBook = "删除本书"
    case move     = "移至分组"
}

