//
//  MBProgressHUD+Hud.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2021/11/5.
//  Copyright © 2021 wangws1990. All rights reserved.
//

import UIKit

extension MBProgressHUD{
    public static func showHUD(_ mainView :UIView,canTap :Bool = true,animated :Bool = false){
        let hud = MBProgressHUD.showAdded(to: mainView, animated: animated)
        hud.contentColor = UIColor.white
        hud.mode = .indeterminate
        hud.bezelView.layer.cornerRadius = 5
        hud.bezelView.style = .solidColor
        hud.animationType = .fade
        hud.bezelView.color = UIColor.black
        hud.removeFromSuperViewOnHide = true
        hud.isUserInteractionEnabled = !canTap
        hud.bezelView.color = UIColor(red: 83/255.0, green: 83/255.0, blue: 83/255.0, alpha: 1.0)
    }
    public static func hideHUD(_ mainView :UIView,animated :Bool = false){
        MBProgressHUD.hide(for: mainView, animated: animated)
    }
}
