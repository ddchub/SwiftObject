//
//  UIImage+Color.swift
//  MySwiftObject
//
//  Created by 王炜圣 on 2021/8/12.
//  Copyright © 2021 wangws1990. All rights reserved.
//

import UIKit
extension UIImage{
    ///MARK:图片换颜色
    func imageColor(_ tintColor : UIColor = moya.appColor) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        if let context = UIGraphicsGetCurrentContext()  {
            context.translateBy(x: 0, y: self.size.height)
            context.scaleBy(x: 1.0, y: -1.0)
            context.setBlendMode(.normal)
            let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
            context.clip(to: rect, mask: self.cgImage!)
            tintColor.setFill()
            context.fill(rect)
        }
        if let newImage = UIGraphicsGetImageFromCurrentImageContext(){
            return newImage
        }
        return UIImage()
    }
    static func placeholder() ->UIImage{
        return UIImage.imageWithColor(color:UIColor.appxf4f4f4())
    }
}
