//
//  GKMySetController.swift
//  MySwiftObject
//
//  Created by anscen on 2022/5/18.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit
import SwiftUI

private let night = "夜间模式"
private let about = "关于我们"
private let logo  = "更换图标"
private let color = "色盘选择"

class GKMySetController: BaseTableViewController {
    private var config :((_ name :String)->())? = nil
    convenience init(config :((_ name :String)->())? = nil) {
        self.init()
        self.config = config
    }
    lazy var listData: [GKMyModel] = {
        return []
    }()
    override func goBack() {
        super.goBack()
        guard let config = config else {
            return
        }
        config("数据回传")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showNavTitle(title: "设置")
        self.setupRefresh(scrollView: self.tableView, options: .auto)
    }
    override func refreshData(page: Int) {
        let model = GKMyModel(title:color, icon:"", subTitle: "")
        let model1 = GKMyModel(title:about, icon:"", subTitle: "")
        let model2 = GKMyModel(title:logo, icon:"", subTitle: "")
        let model3 = GKMyModel(title:night, icon:"", switchOn: moya.night)
        self.listData = [model,model1,model2,model3]
        
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listData.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = GKMySetCell.cellForTableView(tableView: tableView, indexPath: indexPath)
        cell.delegate = self
        let model = self.listData[indexPath.row];
        cell.model = model
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true);
        let model  = self.listData[indexPath.row]
        if model.title == about{
            let vc = UIHostingController(rootView: GKMyAboutView())
            vc.hidesBottomBarWhenPushed = true
            vc.showNavTitle(title: about)
            self.navigationController?.pushViewController(vc, animated: true)
        }else if model.title == logo{
            let controller = tableView.topViewController()
            debugPrint(controller as Any)
            GKJump.jumpToLogoController()
        }else if model.title == color{
            self.navigationController?.pushViewController(GKColorSelectController(), animated: true)
        }
    }
}
extension GKMySetController : GKMySwitchDelegate{
    func switchAction(sender :UISwitch,model :GKMyModel){
        let night = sender.isOn
        ApiClient.setNight(night)
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate,let window = appDelegate.window{
            window.overrideUserInterfaceStyle = night ? .dark : .light
            NotificationCenter.default.post(name: AppThemeNotification, object:UIColor(hex:"000000"))
        }
        self.refreshData(page: 1)
    }
}
