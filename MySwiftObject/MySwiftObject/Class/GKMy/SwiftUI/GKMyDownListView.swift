//
//  GKMyDownListView.swift
//  MySwiftObject
//
//  Created by anscen on 2022/5/19.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import SwiftUI

struct GKMyDownContent: HandyJSON,Hashable {
    var bookId   :String = ""
    var chapterId:String = ""
    var title    :String = ""
    var content  :String = ""
    var created  :String = ""
    var updated  :String = ""
    
    var isVip    :Bool = false
    var getContent :String{
        return removeLine(content:content)
    }
    func removeLine(content:String) ->String{
        var datas : NSArray = content.components(separatedBy: CharacterSet.newlines) as NSArray;
        let pre = NSPredicate(format:"self <> ''");
        datas = datas.filtered(using: pre) as NSArray
        var list : [String] = [];
        datas.forEach { (object) in
            if let str = object as? String{
                let strName = str.trimmingCharacters(in: .whitespacesAndNewlines)
                if strName.count > 0 {
                    let emptyData = "\u{3000}" + "\u{3000}" + strName
                    list.append(emptyData)
                }
            }
        }
        return list.joined(separator: "\n");
    }
    mutating func mapping(mapper: HelpingMapper) {
        mapper <<< self.chapterId <-- ["chapterId","id",]
        mapper <<< self.content   <-- ["cpContent","body","content"]
    }
}
struct GKMyDownListView: View {
    @State private var listData :[GKMyDownContent] = []
    var body: some View {
        VStack {
            List(listData, id: \.self) { objc in
                Button {
                    GKJump.jumpToDownContent(content: objc)
                } label: {
                    GKMyDownContentCell(title: objc.title, vip: false)
                }
//                NavigationLink {
//                    GKMyDownContentView(model: objc)
//                } label: {
//
//                }.padding(.zero)
            }.listStyle(.plain)
        }.onAppear {
            self.loadData()
        }
    }
    func loadData(){
        DispatchQueue.global().async {
            let list = GKRegular.txtBook()
            DispatchQueue.main.async {
                self.listData = list
            }
        }
    }
}
struct GKMyDownContentView: View {
    @State var model   :GKMyDownContent
    var body: some View {
        VStack {
            ScrollView {
                VStack {
                    Text(self.model.getContent).font(Font.system(size: 26)).foregroundColor(Color(UIColor(hex: "CFDFD7"))).padding(EdgeInsets(top: 20, leading: 20, bottom: 20, trailing: 20)).lineSpacing(5)
                }.padding(.zero)
            }.padding(.zero)
        }.onAppear {
            debugPrint(self.model.getContent)
            self.loadData()
        }
    }
    func loadData(){

    }
}
struct GKMyDownContentCell: View {
    var title :String
    var vip :Bool
    var body :some View{
        VStack {
            HStack(alignment: .center, spacing: 0) {
                Text(title).font(Font.system(size: 16, weight: .regular, design: .default)).foregroundColor(Color(UIColor.appx333333())).lineLimit(1)
                Spacer()
                Text(vip ? "Vip" : "").padding(EdgeInsets.init(top: 0, leading: 0, bottom: 0, trailing: 15)).font(Font.system(size: 14)).foregroundColor(.red)
            }.padding(EdgeInsets(top: 15, leading: 0, bottom: 15, trailing: 0))
        }
    }
}

//struct GKMyDownListView_Previews: PreviewProvider {
//    static var previews: some View {
//        GKMyDownListView()
//    }
//}
