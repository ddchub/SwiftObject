//
//  GKMyController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/11.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

private let bookCase   :String = "我的书架"
private let bookBrowse :String = "浏览记录"
private let down       :String = "我的下载"
private let theme      :String = "收藏夹"
private let set        :String = "设置"

class GKMyController: BaseTableViewController {
    private lazy var listData: [GKMyModel] = []
    private lazy var headView : GKMyHeadView = GKMyHeadView.instanceView()
    override func viewDidLoad() {
        super.viewDidLoad()
        let headView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 130))
        headView.addSubview(self.headView)
        self.headView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        self.tableView.tableHeaderView = headView
        self.setupRefresh(scrollView: self.tableView, options:.auto)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showNavTitle(title: "我的")
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.headView.user = moya.user
    }
    override func refreshData(page: Int) {
        let model2 = GKMyModel(title:bookBrowse, icon: "icon_historys", subTitle:"")
        let model3 = GKMyModel(title:down, icon: "icon_down", subTitle:"")
        let model4 = GKMyModel(title:theme, icon: "icon_fav", subTitle:"")
        let model6 = GKMyModel(title:set, icon: "icon_option")
        self.listData = [model2,model3,model4,model6]
        self.tableView.reloadData()
        self.endRefreshFailure()
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listData.count;
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = GKMyTableViewCell.cellForTableView(tableView: tableView, indexPath: indexPath)
        let model = self.listData[indexPath.row];
        cell.model = model
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true);
        let model  = self.listData[indexPath.row];
        switch model.title {
        case bookCase:
            break;
        case bookBrowse:
            GKJump.jumpToBrowse();
            break;
        case theme:
            GKJump.jumpToFavController()
            break
        case down:
            
            GKJump.jumpToDownView()
            break
        case set:
            GKJump.jumpToSetController()
            break
        default:
            break
        }
    }
}

