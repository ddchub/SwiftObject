//
//  GKMyTableViewCell.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/11.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
protocol GKMySwitchDelegate :NSObjectProtocol {
    func switchAction(sender :UISwitch)
}
class GKMyTableViewCell: BaseTableCell {

    @IBOutlet weak var imageRight: UIImageView!
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var lineView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLab.textColor = UIColor.appx333333()
        self.lineView.backgroundColor = UIColor.appxe5e5e5()
    }
    var model : GKMyModel?{
        didSet{
            guard let item = model else { return }
            self.titleLab.text = item.title
            self.imageV.image = UIImage(named: item.icon)
        }
    }
}
