//
//  GKMyLoginController.swift
//  MySwiftObject
//
//  Created by anscen on 2022/3/31.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit

class GKMyLogoController: BaseConnectionController {
    private lazy var listData: [GKMyLogo] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showNavTitle(title: "更换图标")
        self.setupRefresh(scrollView: self.collectionView, options: .auto)
    }
    override func refreshData(page: Int) {
        let item1 = GKMyLogo(name: "icon1", icon: "icon_appstore60x60")
        let item2 = GKMyLogo(name: "icon2", icon: "icon_fancy60x60")
        let item3 = GKMyLogo(name: nil, icon: "AppIcon")
        self.listData = [item3,item1,item2]
        self.collectionView.reloadData()
        self.endRefresh(more: false)
    }
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listData.count
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: SCREEN_WIDTH/3 - 1, height:120)
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = self.listData[indexPath.row]
        let cell = GKMyLogoCell.cellForCollectionView(collectionView: collectionView, indexPath: indexPath)
        cell.imageV.image = UIImage(named: item.icon)
        cell.check.isHidden = item.name == UIApplication.shared.alternateIconName ? false : true
        return cell
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = self.listData[indexPath.row]
        self.changeIcon(item.name)
        self.collectionView.reloadData()
    }
    func changeIcon(_ nickName :String? = nil){
        if UIApplication.shared.supportsAlternateIcons{
            UIApplication.shared.setAlternateIconName(nickName) { error in
                debugPrint(error?.localizedDescription ?? "")
            }
        }
    }
}
struct GKMyLogo {
    var name :String? = nil
    var icon :String
}
