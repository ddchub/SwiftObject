//
//  GKMyDartController.swift
//  MySwiftObject
//
//  Created by anscen on 2022/6/16.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit
private let system = "跟随系统"
private let normal = "普通模式"
private let dart   = "夜间模式"
class GKMyDartController: BaseTableViewController {

    private lazy var listData :[[GKDartModel]] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showNavTitle(title: "深色模式")
        self.setupRefresh(scrollView: self.tableView, options: .auto)
    }
    override func refreshData(page: Int) {
        let item1 = GKDartModel(title:system, subTitle: "开启后,应用内的模式将和系统保持一致", switchOn: moya.client.dart == .system ? true : false)
        let item2 = GKDartModel(title: normal, subTitle: "", switchOn: nil,dart:.light)
        let item3 = GKDartModel(title: dart, subTitle: "", switchOn: nil,dart: .dart)

        self.listData = moya.client.dart == .system ? [[item1]] : [[item1],[item2,item3]]
        self.tableView.reloadData()
        self.endRefresh(more: false)
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.listData.count
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listData[section].count
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let mainView = UIView()
        mainView.isHidden = section == 0
        mainView.backgroundColor = UIColor.appxf4f4f4()
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.appx666666()
        mainView.addSubview(label)
        label.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(16)
            make.centerY.equalToSuperview()
        }
        label.text = "手动设置"
        return mainView
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0 :30
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.listData[indexPath.section][indexPath.row]
        if let _ = item.switchOn{
            let cell = GKMySetSystemCell.cellForTableView(tableView: tableView, indexPath: indexPath)
            cell.dartModel = item
            cell.delegate = self
            return cell
        }
        let cell = GKMySetCell.cellForTableView(tableView: tableView, indexPath: indexPath)
        cell.dartModel = item
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = self.listData[indexPath.section][indexPath.row]
        if let _ = item.switchOn{
            return
        }
        if item.title == normal{
            self.setDart(dart: .light)
        }else{
            self.setDart(dart: .dart)
        }
    }
    func setDart(dart :GKDartState){
        if dart != moya.client.dart{
            ApiClient.setDart(dart)
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate,let window = appDelegate.window{
                window.overrideUserInterfaceStyle = moya.interfaceStyle
                NotificationCenter.default.post(name: AppThemeNotification, object:UIColor(hex:"000000"))
            }
            self.refreshData(page: 1)
        }
    }
}
extension GKMyDartController : GKMySwitchDelegate{
    func switchAction(sender :UISwitch){
        if sender.isOn{
            self.setDart(dart: .system)
        }else{
            self.setDart(dart: .light)
        }
        self.refreshData(page: 1)
    }
}


