//
//  GKMySetSystemCell.swift
//  MySwiftObject
//
//  Created by anscen on 2022/6/16.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit

class GKMySetSystemCell: BaseTableCell {
    weak var delegate :GKMySwitchDelegate? = nil
    @IBOutlet weak var switchBtn: UISwitch!
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var subTitleLab: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLab.textColor = UIColor.appx333333()
        self.subTitleLab.textColor = UIColor.appx999999()
        self.switchBtn.tintColor = UIColor.appColor()
    }
    var dartModel :GKDartModel?{
        didSet{
            guard let item = dartModel else { return }
            self.titleLab.text = item.title
            self.subTitleLab.text = item.subTitle
            self.switchBtn.isHidden = true
            if let switchs = item.switchOn{
                self.switchBtn.isHidden = false
                self.switchBtn.isOn = switchs
            }
        }
    }
    @IBAction func switchAction(_ sender: UISwitch) {
        guard let delegate = self.delegate else { return }
        delegate.switchAction(sender: sender)
    }
}
