//
//  GKHomeTabController.swift
//  MySwiftObject
//
//  Created by anscen on 2022/7/22.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit

class GKHomeTabController: BaseViewController {
    
    private lazy var listControllers :[BaseViewController] = [GKHomeWallController(),GKClassifyTabController()]
    private lazy var magicViewCtrl: VTMagicController = {
        let ctrl = VTMagicController()
        ctrl.magicView.navigationInset = UIEdgeInsets(top: STATUS_BAR_HIGHT, left: 0, bottom: 0, right: 0)
        ctrl.magicView.switchStyle = .default;
        ctrl.magicView.layoutStyle = .center
        ctrl.magicView.itemSpacing = 40
        ctrl.magicView.navigationHeight = NAVI_BAR_HIGHT
        ctrl.magicView.sliderHeight = 2
        ctrl.magicView.sliderOffset = -3
        ctrl.magicView.isAgainstStatusBar = false;
        ctrl.magicView.dataSource = self;
        ctrl.magicView.delegate = self;
        ctrl.magicView.itemScale = 1;
        ctrl.magicView.needPreloading = true;
        ctrl.magicView.bounces = true;
        ctrl.magicView.isScrollEnabled = true
        ctrl.magicView.sliderStyle = .default
        ctrl.magicView.isMenuScrollEnabled = true
        return ctrl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadUI()
    }
    override func loadBaseUI() {
        super.loadBaseUI()
        self.overrideUserInterfaceStyle = moya.interfaceStyle
        self.listControllers.forEach { vc in
            vc.overrideUserInterfaceStyle = moya.interfaceStyle
        }
    }
    private func loadUI(){
        self.fd_prefersNavigationBarHidden = true
        self.magicViewCtrl.magicView.backgroundColor = UIColor.systemBackground
        self.magicViewCtrl.magicView.separatorColor = UIColor.systemBackground
        self.magicViewCtrl.magicView.navigationColor = UIColor.systemBackground
        self.magicViewCtrl.magicView.backgroundColor = UIColor.systemBackground
        self.magicViewCtrl.magicView.sliderColor = UIColor.appColor()
        self.addChild(self.magicViewCtrl);
        self.view.addSubview(self.magicViewCtrl.view);
        self.magicViewCtrl.view.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.magicViewCtrl.magicView.reloadData()
        }
    }
}
extension GKHomeTabController :VTMagicViewDelegate,VTMagicViewDataSource{
    func menuTitles(for magicView: VTMagicView) -> [String] {
        return ["壁纸","小说"]
    }
    func magicView(_ magicView: VTMagicView,menuItemAt: UInt) -> UIButton {
        let button = magicView.dequeueReusableItem(withIdentifier: "com.new.btn.itemIdentifier") ?? UIButton(type: .custom)
        button.setTitleColor(UIColor.appx333333(), for: .normal)
        button.setTitleColor(UIColor.appColor(), for: .selected)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        return button
    }
    func magicView(_ magicView: VTMagicView, viewControllerAtPage pageIndex: UInt) -> UIViewController {
        return self.listControllers[Int(pageIndex)]
    }
}
