//
//  ApiMoyaTask.swift
//  MySwiftObject
//
//  Created by anscen on 2022/1/10.
//  Copyright © 2022 wangws1990. All rights reserved.
//
//
import UIKit
import Alamofire
import Moya

extension ApiMoya{
    public var task: Task {
        var params :[String :Any] = [:]
        switch self {
        case let .search(keyword, page):
            params = ["query" :keyword,"start" : (page - 1),"limit" :RefreshPageSize]
            break
        case let .classifyTail(group, name: name, page: page):
            params = ["gender":group,"type":"hot","major":name,"start":String((page - 1)*RefreshPageSize + 1),"limit":String(RefreshPageSize),"minor":""];
            break
        case let .bookUpdate(bookId):
            params = ["id":bookId,"view":"updated"]
        case let .source(bookId):
            params = ["book":bookId,"view":"summary"]
        case .chapters:
            params = ["view":"chapters"]
        case let .mandetail(comicid):
            params = ["comicid":comicid]
        case let .manchapter(chapterId):
            params = ["chapter_id":chapterId]
        case let .homeWall(page):
            params = ["key":moya.wallkey,"lang":"zh","image_type":"photo","page":page,"q":""]
        default:
            break
        }
        return .requestParameters(parameters: params, encoding: URLEncoding.default)
    }
}
