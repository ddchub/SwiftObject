//
//  ApiModel.swift
//  MySwiftObject
//
//  Created by anscen on 2022/1/10.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit
import SwiftyJSON

public class ApiModel : HandyJSON{
    var code   :Int          = 0
    var msg    :String        = ""
    var data   :[String:Any]  = ["":""]
    required public init() {}
}
public class ApiPageModel : HandyJSON{
    var ok        :Bool  = false
    var total     :Int   = 0
    var list      :[Any] = []
//    var page      :Int   = 0
//    var totalPage :Int   = 0
    required public init() {}
    public func mapping(mapper: HelpingMapper) {
        mapper <<< self.list <-- ["list","books"]
    }
}

