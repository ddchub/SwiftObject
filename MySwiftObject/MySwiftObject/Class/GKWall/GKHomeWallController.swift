//
//  GKHomeWallController.swift
//  MySwiftObject
//
//  Created by anscen on 2022/7/22.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit

class GKHomeWallController: BaseTableViewController {
    private lazy var listData :[GKHomeWall] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupRefresh(scrollView: self.tableView, options: .defaults)
    }
    override func refreshData(page: Int) {
        ApiMoya.request(target: .homeWall(page)) { json in
            if page == 1{
                self.listData.removeAll()
            }
            if let list = [GKHomeWall].deserialize(from: json["hits"].rawString()) as? [GKHomeWall]{
                self.listData.append(contentsOf: list)
                self.tableView.reloadData()
                self.endRefresh(more: list.count > 0)
            }
        } failure: { error in
            self.endRefreshFailure(error: error)
        }
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listData.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = GKHomeWallCell.cellForTableView(tableView: tableView, indexPath: indexPath)
        cell.model = self.listData[indexPath.row]
        return cell
    }
}
