//
//  GKHomeWallCell.swift
//  MySwiftObject
//
//  Created by anscen on 2022/7/22.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit

class GKHomeWallCell: BaseTableCell {
    @IBOutlet weak var imageV: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = UIColor.black
    }
    var model :GKHomeWall?{
        didSet{
            guard let item = model else { return }
            self.imageV.setWallImageWithURL(url: item.largeImageURL ?? "")
        }
    }
}
