//
//  GKHomeWall.swift
//  MySwiftObject
//
//  Created by anscen on 2022/7/22.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit
import HandyJSON

class GKHomeWall: HandyJSON {
    var id :String?            = ""
    var largeImageURL:String? = ""
    var webformatURL :String? = ""
    var previewURL   :String? = ""
    var pageURL      :String? = ""
    var userImageURL :String? = ""
    var tags         :String? = ""
    
    required init() {}
}
