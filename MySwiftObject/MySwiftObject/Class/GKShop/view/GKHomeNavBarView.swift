//
//  GKHomeNavBarView.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/5.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKHomeNavBarView: BaseView {
    @IBOutlet weak var chirdenBtn: UIButton!
    @IBOutlet weak var tapView: UIView!
    @IBOutlet weak var mainView: UIView!

    override func awakeFromNib() {
        self.mainView.layer.masksToBounds = true;
        self.mainView.layer.cornerRadius = 4.0;
        self.mainView.backgroundColor = UIColor.appxffffff()
        self.tapView.backgroundColor = UIColor.appxffffff()
        self.chirdenBtn.setTitle("列表", for: .normal)
        self.chirdenBtn.setTitle("列表", for: [.normal,.highlighted])
        
        self.chirdenBtn.setTitle("九宫格", for: .selected)
        self.chirdenBtn.setTitle("九宫格", for: [.selected,.highlighted])
        self.chirdenBtn.contentEdgeInsets = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
        self.chirdenBtn.layer.masksToBounds = true
        self.chirdenBtn.layer.borderWidth = 1
        self.chirdenBtn.layer.borderColor = UIColor(hex: "dddddd").cgColor
        self.chirdenBtn.layer.cornerRadius = 5
        self.reloadUI();
    }
    public func reloadUI(){
        self.chirdenBtn.isSelected = moya.listView ? false : true
        
//        self.chirdenBtn.isSelected = (moya.client.sex == .boy) ? false : true;
    }
}

