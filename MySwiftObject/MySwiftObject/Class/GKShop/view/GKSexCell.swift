//
//  GKSexCell.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/17.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
//
class GKSexCell: BaseCollectionCell {

    @IBOutlet weak var titleLab: UILabel!
    var model :GKRankModel?{
        didSet{
            guard let item = model else { return }
            self.titleLab.text = item.shortTitle ?? "";
            selectColor(select: item.select!)
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectColor(select: false)
    }
    private func selectColor(select :Bool){
        self.titleLab.layer.masksToBounds = true
        self.titleLab.layer.cornerRadius = 5
        if (select){
            self.titleLab.textColor = moya.appColor
            self.titleLab.backgroundColor = UIColor(hex: "EBF5FF")
        }else{
            self.titleLab.textColor = UIColor.appx999999()
            self.titleLab.backgroundColor = UIColor(hex: "ededed")
        }
    }
}
