//
//  GKShopTabController.swift
//  MySwiftObject
//
//  Created by anscen on 2022/2/24.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit

class GKShopTabController: BaseViewController {
    convenience init(listData :[GKRankModel]) {
        self.init()
        self.listData = listData
    }
    private var listData :[GKRankModel]? = nil
    private var listTitles :[String] = []
    private var listControllers :[GKShopItemController] = []
    private lazy var magicViewCtrl: VTMagicController = {
        let ctrl = VTMagicController()
        
        ctrl.magicView.navigationInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        ctrl.magicView.switchStyle = .default;
        
        ctrl.magicView.layoutStyle = .default
        ctrl.magicView.itemSpacing = 25
        ctrl.magicView.navigationHeight = 40
        ctrl.magicView.sliderHeight = 2
        ctrl.magicView.sliderOffset = 0
        ctrl.magicView.isAgainstStatusBar = false;
        ctrl.magicView.dataSource = self;
        ctrl.magicView.delegate = self;
        ctrl.magicView.itemScale = 1;
        ctrl.magicView.needPreloading = true;
        ctrl.magicView.bounces = true;
        ctrl.magicView.isScrollEnabled = true
        ctrl.magicView.sliderStyle = .bubble
        ctrl.magicView.bubbleInset = UIEdgeInsets(top: 3, left: 8, bottom: 3, right: 8)
        ctrl.magicView.bubbleRadius = 10
        ctrl.magicView.isMenuScrollEnabled = true
        return ctrl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadUI()
        self.loadData()
    }
    override func loadBaseUI() {
        super.loadBaseUI()
        self.overrideUserInterfaceStyle = moya.interfaceStyle
        self.listControllers.forEach { vc in
            vc.overrideUserInterfaceStyle = moya.interfaceStyle
        }
    }
    private func loadUI(){
        self.magicViewCtrl.magicView.backgroundColor = UIColor.systemBackground
        self.magicViewCtrl.magicView.separatorColor = UIColor.systemBackground
        self.magicViewCtrl.magicView.navigationColor = UIColor.systemBackground
        self.magicViewCtrl.magicView.backgroundColor = UIColor.systemBackground
        self.magicViewCtrl.magicView.sliderColor = UIColor.appxf1f2f4()
        self.addChild(self.magicViewCtrl);
        self.view.addSubview(self.magicViewCtrl.view);
        self.magicViewCtrl.view.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    private func loadData(){
        self.listData?.forEach({ item in
            self.listTitles.append(item.shortTitle ?? "")
            let vc = GKShopItemController(rankId: item.rankId ?? "")
            self.listControllers.append(vc)
        })
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.magicViewCtrl.magicView.reloadData()
        }
    }
}
extension GKShopTabController :VTMagicViewDelegate,VTMagicViewDataSource{
    func menuTitles(for magicView: VTMagicView) -> [String] {
        return self.listTitles;
    }
    func magicView(_ magicView: VTMagicView,menuItemAt: UInt) -> UIButton {
        let button = magicView.dequeueReusableItem(withIdentifier: "com.new.btn.itemIdentifier") ?? UIButton(type: .custom)
        button.setTitleColor(UIColor.appx666666(), for: .normal)
        button.setTitleColor(UIColor.appx333333(), for: .selected)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        return button
    }
    func magicView(_ magicView: VTMagicView, viewControllerAtPage pageIndex: UInt) -> UIViewController {
        return self.listControllers[Int(pageIndex)]
    }
}
