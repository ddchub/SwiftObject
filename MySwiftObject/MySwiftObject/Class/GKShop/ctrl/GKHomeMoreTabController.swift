//
//  GKHomeMoreTabController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/10.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKHomeMoreTabController: BaseViewController {

    convenience init(info : GKHomeInfo) {
        self.init()
        self.bookInfo = info
    }
    private var bookInfo:GKHomeInfo? = nil
    private lazy var listData : [String] = []
    private lazy var controllers : [BaseViewController] = []
    private lazy var magicViewCtrl: VTMagicController = {
        let ctrl = VTMagicController();
        ctrl.magicView.separatorHeight = 0.5;
        ctrl.magicView.backgroundColor = UIColor.clear
        ctrl.magicView.separatorColor = UIColor.clear;
        ctrl.magicView.navigationColor = UIColor.clear
        ctrl.magicView.switchStyle = .default;
        
        ctrl.magicView.sliderColor = moya.appColor
        ctrl.magicView.sliderExtension = 1;
        ctrl.magicView.bubbleRadius = 1;
        ctrl.magicView.sliderWidth = 25;
        
        ctrl.magicView.layoutStyle = .center;
        ctrl.magicView.navigationHeight = 45;
        ctrl.magicView.sliderHeight = 2;
        ctrl.magicView.itemSpacing = 20;
        
        ctrl.magicView.isAgainstStatusBar = false;
        ctrl.magicView.dataSource = self;
        ctrl.magicView.delegate = self;
        ctrl.magicView.needPreloading = true;
        ctrl.magicView.bounces = false;
        return ctrl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addChild(self.magicViewCtrl)
        self.view.addSubview(self.magicViewCtrl.view)
        self.magicViewCtrl.view.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        if let model = self.bookInfo,let rankModel = model.rankModel{
            self.showNavTitle(title: model.title)
            if let id = rankModel.rankId,id.count > 0{
                self.listData.append("周榜")
                let vc = GKClassifyTailController(rankId: id)
                self.controllers.append(vc)
            }
            if let id = rankModel.monthRank,id.count > 0{
                self.listData.append("月榜")
                let vc = GKClassifyTailController(rankId: id)
                self.controllers.append(vc)
            }
            if let id = rankModel.totalRank,id.count > 0{
                self.listData.append("总榜")
                let vc = GKClassifyTailController(rankId: id)
                self.controllers.append(vc)
            }
        }
        self.magicViewCtrl.magicView.navigationHeight = self.listData.count > 1 ? 45 : 0.0
        self.magicViewCtrl.magicView.reloadData()
    }
}
extension GKHomeMoreTabController :VTMagicViewDataSource,VTMagicViewDelegate{
    func menuTitles(for magicView: VTMagicView) -> [String] {
        return self.listData
    }
    func magicView(_ magicView: VTMagicView, menuItemAt itemIndex: UInt) -> UIButton {
        let button  = magicView.dequeueReusableItem(withIdentifier: "com.home.btn.itemIdentifier") ?? UIButton(type: .custom)
        button.setTitle(self.listData[Int(itemIndex)], for: .normal)
        button.setTitleColor(UIColor.appx333333(), for: .normal)
        button.setTitleColor(UIColor.appx333333(), for: .selected)
        button.titleLabel?.font = UIFont.systemFont(ofSize:16, weight: .regular)
        return button
    }
    func magicView(_ magicView: VTMagicView, viewControllerAtPage pageIndex: UInt) -> UIViewController {
        return self.controllers[Int(pageIndex)]
    }
}
