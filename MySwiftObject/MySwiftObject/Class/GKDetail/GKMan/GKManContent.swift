//
//  GKManContent.swift
//  MySwiftObject
//
//  Created by anscen on 2022/1/18.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit
import HandyJSON

class GKManContent: HandyJSON {
    var chapterId  :String = ""
    var name       :String = ""
    var status     :Int    = 0
    var type       :Int    = 0
    
    var image_list :[GKManItem]? = nil
    
    var contentHeight : CGFloat{
        get{
            var height :CGFloat = 0
            if let list = image_list{
                list.forEach { objc in
                    height = objc.cellHeight + height
                }
            }
            return height
        }
    }
    required init() {}
    func mapping(mapper: HelpingMapper) {
        mapper <<< self.chapterId <-- ["chapter_id","chapterId"]
    }
}
class GKManItem :HandyJSON{
    var  image_id :String = ""
    var  location :String = ""
    var  img05    :String = ""
    var  image50  :String = ""
    var  webp :Bool    = false
    var  type :Bool    = false
    var width :Float   = 0
    var height:Float   = 0
    
    var cellHeight :CGFloat{
        return CGFloat(self.height)*moya.safe_width/CGFloat(self.width)
    }
    required init() {}
}
