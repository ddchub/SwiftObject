//
//  GKCenterController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2020/3/31.
//  Copyright © 2020 wangws1990. All rights reserved.
//

import UIKit

class GKCenterController: BaseConnectionController {
    convenience init(bookId :String, model :GKBookInfo? = nil) {
        self.init()
        self.bookId = bookId
        self.model = model
    }
    private lazy var listData: [GKBook] = []
    private lazy var titleLab: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize:12)
        label.numberOfLines = 2
        label.textColor = UIColor.appx666666()
        
        return label
    }()
    public var model  : GKBookInfo?{
        didSet{
            self.collectionView.reloadData()
        }
    }
    private var bookId : String? = ""
    private let top :CGFloat = 15
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupRefresh(scrollView: self.collectionView, options:[.header,.auto])
        self.collectionView.backgroundColor = UIColor.appxffffff()
        self.collectionView.backgroundView?.backgroundColor = UIColor.appxffffff()
    }
    override func refreshData(page: Int) {
        guard let id = self.bookId else { return endRefresh(more: false) }
        ApiMoya.request(target: .commend(id), model: GKBook.self) { object in
            if page == 1{
                self.listData .removeAll()
            }
            if let list = object as? [GKBook]{
                self.listData.append(contentsOf: list)
            }
            self.collectionView.reloadData();
            self.endRefresh(more: object.count > 0)
        } failure: { error in
            self.endRefreshFailure(error: error)
        }
    }
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1;
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listData.count;
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0;
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0;
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: SCREEN_WIDTH, height: self.model != nil ? GKDetailReusableView.getHeight(model: self.model!) : 0.00)
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = GKDetailReusableView.viewForCollectionView(collectionView: collectionView, elementKind: kind, indexPath: indexPath)
        view.isHidden = self.model == nil
        if self.model != nil {
            view.contentLab.numberOfLines = self.model!.numberLine
            view.content = self.model?.shortIntro
            view.moreBtn.isSelected = self.model?.numberLine == 0
        }
        view.moreBtn.addTarget(self, action: #selector(moreAction(sender:)), for: .touchUpInside)
        return view
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return GKBookCell.inset
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return GKBookCell.cellSize
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = GKBookCell.cellForCollectionView(collectionView: collectionView, indexPath: indexPath)
        cell.model = self.listData[indexPath.row] ;
        return cell;
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = self.listData[indexPath.row] ;
        GKJump.jumpToDetail(bookId: model.bookId);
    }
    @objc func moreAction(sender : UIButton){
        self.model?.numberLine = self.model?.numberLine == 3 ? 0 : 3
    
        self.collectionView.reloadData()
    }
    override var refreshVertica: CGFloat{
        return -200
    }
}
