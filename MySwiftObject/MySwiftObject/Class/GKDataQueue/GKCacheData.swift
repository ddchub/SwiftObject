//
//  GKBookCacheDataQueue.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/11.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
import SwiftyJSON
//书本文章缓存
class GKCacheData {
    public static func listChapter(bookId :String,sucesss:@escaping ((_ listChapters : [JSON]) ->())){
        bookSource(bookId: bookId) { model in
            bookChapters(sourceId: model.sourceId) { object in
                sucesss(object.array ?? [])
                GKHistoryData.updateChapterDatas(bookId: bookId, chapters: object.rawString()) { success in
                    
                }
            } failure: { error in
                sucesss([])
            }
        } failure: { error in
            sucesss([])
        }
    }
    public static func getCache(bookId :String,
                                json  :JSON,
                                sucesss:@escaping ((_ object : GKContent) ->()),
                                failure:@escaping ((_ error : String) ->())){
        guard let chap = GKChapter.deserialize(from: json.rawString()) else { return }
        GKCacheData.getContent(bookId:bookId, chapterId: chap.chapterId) { (content) in
            if let item = content {
                sucesss(item)
            }else{
                bookContent(chap.link, sucesss: { model in
                    model.bookId = bookId
                    model.chapterId = chap.chapterId
                    sucesss(model)
                    if model.content.count > 0{
                        GKCacheData.insertContent(bookId:bookId, content: model, completion: { (success) in
                            
                        })
                    }
                }, failure: failure)
            }
        };
    }
    private static func insertContent(bookId:String, content :GKContent,completion:@escaping ((_ success : Bool) -> ())){
        BaseQueue.insertData(tableName: bookId, primaryId: content.chapterId , content: content.toJSONString() ?? "", completion: completion)
    }
    private static func getContent(bookId:String, chapterId:String,completion:@escaping ((_ content : GKContent?) -> Void)){
        BaseQueue.searchData(tableName: bookId, primaryId: chapterId) { (json) in
            guard let data = [GKContent].deserialize(from: json.rawString())else {
                completion(nil)
                return
            }
            completion(data.first as? GKContent)
        }
    }
    private static func dropTable(bookId:String,completion:@escaping ((_ success : Bool) ->Void)){
        BaseQueue.dropTable(bookId,completion:completion)
    }
    
    private static func bookContent(_ link :String,sucesss:@escaping ((_ object : GKContent) ->Void),failure:@escaping ((_ error : String) ->())){
        ApiMoya.request(target: .content(link), sucesss: { json in
            guard let model = GKContent.deserialize(from: json["chapter"].rawString()) else { return failure("") }
            sucesss(model)
        }, failure: failure)
    }
    private static func bookChapters(sourceId :String,
                                     sucesss:@escaping ((_ object : JSON) ->Void),
                                     failure:@escaping ((_ error : String) ->Void)){
        ApiMoya.request(target: .chapters(sourceId), sucesss: { json in
            sucesss(json["chapters"])
        }, failure: failure)
    }
    private static func bookSource(bookId:String,
                                    sucesss:@escaping ((_ source : GKSource) ->()),
                                    failure:@escaping ((_ error : String) ->())){
        ApiMoya.request(target: .source(bookId), sucesss: { json in
            guard let data = [GKSource].deserialize(from: json.rawString()),let item = data.first else {
                return failure("")
            }
            sucesss(item ?? GKSource())
        }, failure: failure)
    }
}
