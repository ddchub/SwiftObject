//
//  GKSearchHistoryCell.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/20.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKSearchHistoryCell: BaseTableCell {
    lazy var imageV : UIImageView = {
        let imageV : UIImageView = UIImageView();
        imageV.image = UIImage(named: "icon_history");
        return imageV;
    }()
    lazy var lineView : UIView = {
        let lineView = UIView()
        lineView.backgroundColor = UIColor.appxe5e5e5()
        return lineView
    }()
    lazy var titleLab : UILabel = {
        let titleLab : UILabel = UILabel()
        titleLab.font = UIFont.systemFont(ofSize: 14);
        titleLab.textColor = UIColor(hex: "666666")
        titleLab.textAlignment = .left
        return titleLab
    }()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier);
        self.contentView.addSubview(self.imageV);
        self.contentView.addSubview(self.titleLab);
        self.contentView.addSubview(self.lineView)
        self.lineView.snp.makeConstraints { make in
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(0.8)
        }
        self.imageV.snp.makeConstraints { (make) in
            make.width.height.equalTo(30);
            make.left.equalToSuperview().offset(15);
            make.centerY.equalToSuperview();
        }
        self.titleLab.snp.makeConstraints { (make) in
            make.centerY.equalToSuperview();
            make.left.equalTo(self.imageV.snp.right).offset(10);
            make.right.equalToSuperview().offset(-15);
        }
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

}
