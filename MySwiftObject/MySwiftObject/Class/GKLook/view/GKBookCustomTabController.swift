//
//  GKBookCustomTabController.swift
//  MySwiftObject
//
//  Created by anscen on 2022/6/17.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit
import Colorful
protocol BookFontColorDelegate :NSObjectProtocol {
    func bookFontColor(color :UIColor,font :Bool)
}
class GKBookCustomTabController: BaseViewController {
    weak var delegate:GKBookSetDelegate? = nil
    private lazy var saveBtn: UIButton = {
        let button = UIButton(type: .custom)
        button.layer.borderWidth = 0.8
        button.layer.cornerRadius = 5
        button.layer.borderColor = UIColor.appxe5e5e5().cgColor
        button.contentEdgeInsets = UIEdgeInsets(top: 6, left: 10, bottom: 6, right: 10)
        button.setTitle("保存", for: .normal)
        button.setTitleColor(UIColor.appx333333(), for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        button.addTarget(self, action: #selector(saveAction), for: .touchUpInside)
        return button
    }()
    private var listTitles :[String] = ["皮肤","字体"]
    private var listControllers :[BaseViewController] = []
    private lazy var titleLab: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.font = UIFont.systemFont(ofSize: 20)
        label.textAlignment = .center
        label.textColor = UIColor(hex: moya.bookSet.theme.fontColor)
        label.backgroundColor = UIColor(hex: moya.bookSet.theme.themeColor)
        label.text = "百日依山尽,黄河入海流。\n欲穷千里目,更上一层楼。"
        return label
    }()
    private lazy var magicViewCtrl: VTMagicController = {
        let ctrl = VTMagicController()
        
        ctrl.magicView.navigationInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        ctrl.magicView.switchStyle = .default;
        
        ctrl.magicView.layoutStyle = .divide
        ctrl.magicView.navigationHeight = 40
        ctrl.magicView.sliderHeight = 2
        ctrl.magicView.sliderOffset = 0
        ctrl.magicView.isAgainstStatusBar = false;
        ctrl.magicView.dataSource = self;
        ctrl.magicView.delegate = self;
        ctrl.magicView.itemScale = 1;
        ctrl.magicView.needPreloading = true;
        ctrl.magicView.bounces = true;
        ctrl.magicView.isScrollEnabled = true
        ctrl.magicView.isMenuScrollEnabled = true
        return ctrl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadUI()
        self.loadData()
    }
    private func loadUI(){
        self.showNavTitle(title: "自定义颜色")
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: self.saveBtn)
        self.magicViewCtrl.magicView.backgroundColor = UIColor.systemBackground
        self.magicViewCtrl.magicView.separatorColor = UIColor.systemBackground
        self.magicViewCtrl.magicView.navigationColor = UIColor.systemBackground
        self.magicViewCtrl.magicView.backgroundColor = UIColor.systemBackground
        self.magicViewCtrl.magicView.sliderColor = UIColor.appColor()
        
        self.view.addSubview(self.titleLab)
        self.titleLab.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(30)
        }
        self.addChild(self.magicViewCtrl);
        self.view.addSubview(self.magicViewCtrl.view);
        self.magicViewCtrl.view.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(self.titleLab.snp.bottom).offset(30)
        }
    }
    @objc private func saveAction(){
        guard let delegate = delegate else {
            return
        }
        let model = moya.bookSet.theme
        model.fontColor = self.titleLab.textColor.hex()
        model.themeColor = self.titleLab.backgroundColor?.hex() ?? "ffffff"
        GKBookSet.setSkin(skin: model)
        delegate.changeSkin?(setView: GKBookSetView())
        self.goBack()
    }
    private func loadData(){
        let theme = GKBookThemeController(delegaet: self, font: false,color:self.titleLab.backgroundColor ?? .white)
        let font = GKBookThemeController(delegaet: self, font: true,color: self.titleLab.textColor ?? .white)
        self.listControllers = [theme,font]
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.magicViewCtrl.magicView.reloadData()
        }
    }
}
extension GKBookCustomTabController :VTMagicViewDelegate,VTMagicViewDataSource{
    func menuTitles(for magicView: VTMagicView) -> [String] {
        return self.listTitles;
    }
    func magicView(_ magicView: VTMagicView,menuItemAt: UInt) -> UIButton {
        let button = magicView.dequeueReusableItem(withIdentifier: "com.new.btn.itemIdentifier") ?? UIButton(type: .custom)
        button.setTitleColor(UIColor.appx666666(), for: .normal)
        button.setTitleColor(UIColor.appColor(), for: .selected)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        return button
    }
    func magicView(_ magicView: VTMagicView, viewControllerAtPage pageIndex: UInt) -> UIViewController {
        return self.listControllers[Int(pageIndex)]
    }
}
extension GKBookCustomTabController :BookFontColorDelegate{
    func bookFontColor(color: UIColor, font: Bool) {
        if font{
            self.titleLab.textColor = color
        }else{
            self.titleLab.backgroundColor = color
        }
    }
}
class GKBookThemeController :BaseViewController{
    convenience init(delegaet :BookFontColorDelegate,font :Bool,color : UIColor) {
        self.init()
        self.delegate = delegaet
        self.font = font
        self.color = color
    }
    private weak var delegate :BookFontColorDelegate? = nil
    private var font :Bool? = nil
    private var color :UIColor? = nil
    lazy var colorPicker: ColorPicker = {
        let colorPicker = ColorPicker()
        colorPicker.addTarget(self, action: #selector(colorAction), for:.valueChanged)
        colorPicker.set(color: .white, colorSpace: .sRGB)
        return colorPicker
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(self.colorPicker)
        self.colorPicker.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.height.width.equalTo(SCREEN_WIDTH - 80)
        }
//        guard let color = color else {
//            return
//        }
//        self.colorPicker.set(color: color, colorSpace: .sRGB)
    }
    @objc func colorAction(){
        guard let delegate = delegate else {
            return
        }
        guard let font = font else {
            return
        }
        delegate.bookFontColor(color: self.colorPicker.color, font: font)
        debugPrint(self.colorPicker.color.hex())
    }
}
