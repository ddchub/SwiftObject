//
//  GKBookSetView.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/12.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit
import MBProgressHUD
@objc protocol GKBookSetDelegate : NSObjectProtocol{
    @objc optional func changeFont(setView:GKBookSetView);
    @objc optional func changeRead(setView:GKBookSetView);
    @objc optional func changeSkin(setView:GKBookSetView);
    @objc optional func changeHalf(setView:GKBookSetView);
}
class GKBookSetView: BaseView,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate {
    weak var delegate:GKBookSetDelegate? = nil
    lazy var listData : [GKBookTheme] = {
        return GKBookTheme.themeColorDatas()
    }()
    lazy var listBrowse: [GKNovelBrowse] = {
        return [.defaults,.pageCurl,.nones,.joined,.scroll];
    }()
    @IBOutlet weak var customBtn: UIButton!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var reduceBtn: UIButton!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var fontLab: UILabel!
    
    @IBOutlet weak var halfBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var browCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.customBtn.layer.masksToBounds = true
        self.customBtn.layer.cornerRadius = 5
        self.customBtn.layer.borderWidth = 0.8
        self.customBtn.layer.borderColor = UIColor.appxe5e5e5().cgColor
        self.tag = 10086
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        self.browCollectionView.dataSource = self
        self.browCollectionView.delegate = self
        
        self.slider.setThumbImage(UIImage(named: "icon_slider"), for: .normal)
        self.isUserInteractionEnabled = true
        let color = UIColor.systemBackground
        self.collectionView.backgroundView?.backgroundColor = color
        self.collectionView.backgroundColor = color
        self.browCollectionView.backgroundView?.backgroundColor = color
        self.browCollectionView.backgroundColor = color
        
        self.reduceBtn.layer.masksToBounds = true;
        self.reduceBtn.layer.cornerRadius = 5;
        self.reduceBtn.layer.borderWidth = 1.0
        self.reduceBtn.layer.borderColor = UIColor.appx999999().cgColor;
        
        self.addBtn.layer.masksToBounds = true;
        self.addBtn.layer.cornerRadius = 5;
        self.addBtn.layer.borderWidth = 1.0
        self.addBtn.layer.borderColor = UIColor.appx999999().cgColor;
        self.loadData();
    }
    
    @IBAction func customAction(_ sender: UIButton) {
        let vc = GKBookCustomTabController()
        vc.delegate = self
        UIViewController.rootTopPresentedController().navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func sliderAction(_ sender: UISlider) {
        let bright :Float = sender.value;
        UIScreen.main.brightness = CGFloat(bright);
    }
    @IBAction func reduceAction(_ sender: UIButton) {
        let model  = moya.bookSet
        if Int(model.fontSize) <= 10{
            MBProgressHUD.showToast("最小字体为10");
            return;
        }
        let font : Int = Int(model.fontSize) - 2
        GKBookSet.setFont(Float(font));
        self.fontLab.text = String(font)+"px";
        guard let delegate = self.delegate else { return }
        delegate.changeFont?(setView: self)
    }
    @IBAction func addAction(_ sender: UIButton) {
        let model  = moya.bookSet
        if Int(model.fontSize) >= 30{
            MBProgressHUD.showToast("最大字体为30");
            return;
        }
        let font : Int = Int(model.fontSize) + 2
        GKBookSet.setFont(Float(font));
        self.fontLab.text = String(font)+"px";
        guard let delegate = self.delegate else { return }
        delegate.changeFont?(setView: self)
    }
    @IBAction func halfAction(_ sender: UIButton) {
        guard let delegate = self.delegate else { return }
        delegate.changeHalf?(setView: self)
    }
    func loadData(){
        let model  = moya.bookSet
        let bright :Float = Float(UIScreen.main.brightness);
        self.slider.value = bright;
        self.fontLab.text = String(model.fontSize)+"px";
        self.collectionView.reloadData()
        self.browCollectionView.reloadData()
        self.halfBtn.isSelected = moya.landscape ? true : false
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1;
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView == self.collectionView  ? self.listData.count : self.listBrowse.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16;
    }
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0;
    }
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12);
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView == self.collectionView ? CGSize(width:45, height: 45) : CGSize(width: 80, height: 30)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        if collectionView == self.collectionView{
            let cell   = GKBookSetCell.cellForCollectionView(collectionView: collectionView, indexPath: indexPath)
            let model  = self.listData[indexPath.row];
            cell.model = model
            return cell
        }
        let cell = GKBookSetBrowseCell.cellForCollectionView(collectionView: collectionView, indexPath: indexPath)
        cell.model  = self.listBrowse[indexPath.row]
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.collectionView == collectionView{
            let model  = self.listData[indexPath.row];
            if model.themeColor == moya.bookSet.theme.themeColor{
                return
            }
            GKBookSet.setSkin(skin: model)
            guard let delegate = self.delegate else { return }
            delegate.changeSkin?(setView: self)
            collectionView.reloadData()
        }else{
            let brow = self.listBrowse[indexPath.row]
            GKBookSet.setBrowse(browse: brow)
            guard let delegate = self.delegate else { return }
            delegate.changeRead?(setView: self)
            collectionView.reloadData()
        }
    }
    
}
class GKBookCollectionViewFlowLayout :UICollectionViewFlowLayout{
    override init() {
        super.init()
        self.scrollDirection = .horizontal
        loadUI()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadUI()
    }
    private func loadUI(){
        self.scrollDirection = .horizontal
    }
}
extension GKBookSetView :GKBookSetDelegate{
    func changeSkin(setView: GKBookSetView) {
        guard let delegate = self.delegate else { return }
        delegate.changeSkin?(setView: self)
        self.collectionView.reloadData()
    }
}
