//
//  GKBookCell.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/9.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class GKBookCell: BaseCollectionCell {
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var titleLab: UILabel!
    @IBOutlet weak var subTitleLab: UILabel!
    @IBOutlet weak var statebtn: UIButton!
    public static var inset : UIEdgeInsets{
        return UIEdgeInsets(top:moya.listTop, left: moya.listView ? 0 : 5, bottom: 5, right: moya.listView ? 0 : 5)
    }
    
    public static var cellSize : CGSize{
        let width = (SCREEN_WIDTH - 10)/3 - 0.1
        return CGSize(width:width, height: (width - 10) * 1.4 + 10 + 65)
    }
    public static var cellSearchSize : CGSize{
        let width = (SCREEN_WIDTH - 10)/3.5 - 0.1
        return CGSize(width:width, height: (width - 10) * 1.4 + 10 + 65)
    }
    var model : GKBook?{
        didSet{
            guard let item = model else { return }
            self.imageV.setGkImageWithURL(url: item.cover ?? "")
            self.titleLab.text = item.title ?? ""
            self.subTitleLab.text = item.author 
            self.statebtn.setTitle(item.minorCate ?? "", for: .normal)
            self.statebtn.isHidden = item.minorCate?.count == 0
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.statebtn.layer.masksToBounds = true;
        self.statebtn.layer.cornerRadius = 5;
        
        self.mainView.layer.masksToBounds = true
        self.mainView.layer.cornerRadius = 5
        self.titleLab.textColor = UIColor.appx333333()
        self.subTitleLab.textColor = UIColor.appx666666()
        self.imageV.image = UIImage.placeholder()
        let layerView = UIView()
        layerView.backgroundColor = UIColor.white
        self.contentView.insertSubview(layerView, at: 0)
        layerView.snp.makeConstraints { make in
            make.edges.equalTo(self.mainView)
        }
        layerView.layer.shadowColor =  UIColor(white: 0, alpha: 0.08).cgColor
        layerView.layer.shadowOffset = CGSize(width: 0, height: 0  )
        layerView.layer.shadowOpacity = 1
        layerView.layer.shadowRadius = 6
        layerView.layer.cornerRadius = 6
    }

}
