//
//  GKFavController.swift
//  MySwiftObject
//
//  Created by anscen on 2022/6/15.
//  Copyright © 2022 wangws1990. All rights reserved.
//

import UIKit

class GKFavController: BaseTableViewController {
    private lazy var listData: [GKBook] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showNavTitle(title: "收藏夹")
        self.setupRefresh(scrollView: self.tableView, options: .auto)
    }
    override func refreshData(page: Int) {
        GKShelfData.getBookModels { datas in
            if page == RefreshPageStart{
                self.listData.removeAll()
            }
            self.listData.append(contentsOf:datas)
            self.tableView.reloadData()
            self.endRefresh(more: false, empty:"收藏夹里面还没有任何小说!")
        }
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listData.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = GKClassifyTailCell.cellForTableView(tableView: tableView, indexPath: indexPath)
        cell.model = self.listData[indexPath.row]
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let model = self.listData[indexPath.row]
        GKJump.jumpToNovel(bookId: model.bookId)
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let model = self.listData[indexPath.row]
        let context = UIContextualAction(style: .destructive, title: "删除") { row, index, finish in
            self.deleteAction(model)
        }
//        let title = model.topTime > 0 ? "取消置顶" : "置顶"
//        let top = UIContextualAction(style: .normal, title:title) { row, index, finish in
//            self.topAction(model)
//        }
        let sw = UISwipeActionsConfiguration(actions: [context])
        sw.performsFirstActionWithFullSwipe = false
        return sw
    }
    func deleteAction(_ model : GKBook){
        let name = model.title ?? ""
        ATAlertView.showAlertView(title: "确定将<\(name)>从收藏夹中移除", message: nil, normals: ["取消"], hights: ["确定"]) { title, index in
            if index > 0{
                GKShelfData.deleteBookModel(bookId: model.bookId ?? "") { success in
                    self.listData.removeAll { item in
                        return item.bookId == model.bookId
                    }
                    self.tableView.reloadData();
                }
            }else{
                self.tableView.reloadData()
            }
        }
    }
}
