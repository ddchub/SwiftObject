//
//  GKClassifyTabController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/5.
//  Copyright © 2018 wangws1990. All rights reserved.
//
import UIKit

class GKClassifyTabController: BaseViewController {
    private lazy var listData  :[String] = ["male","female","press","picture"]
    private lazy var listTitles:[String] = ["男生","女生","出版社","其他"]
    private lazy var controllers:[BaseViewController] = []
    
    private lazy var magicViewCtrl: VTMagicController = {
        let ctrl = VTMagicController()
        ctrl.magicView.navigationInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        ctrl.magicView.switchStyle = .default;
        
        ctrl.magicView.layoutStyle = .default
        ctrl.magicView.itemSpacing = 30
        ctrl.magicView.navigationHeight = 36
        ctrl.magicView.sliderHeight = 2
        ctrl.magicView.sliderOffset = 0
        ctrl.magicView.isAgainstStatusBar = false;
        ctrl.magicView.dataSource = self;
        ctrl.magicView.delegate = self;
        ctrl.magicView.itemScale = 1;
        ctrl.magicView.needPreloading = true;
        ctrl.magicView.bounces = false;
        ctrl.magicView.isScrollEnabled = true
        ctrl.magicView.sliderStyle = .bubble
        ctrl.magicView.bubbleInset = UIEdgeInsets(top: 4, left: 13, bottom: 4, right: 13)
        ctrl.magicView.bubbleRadius = 11
        return ctrl
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadUI();
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func loadBaseUI() {
        super.loadBaseUI()
        self.controllers.forEach { vc in
            vc.overrideUserInterfaceStyle = moya.interfaceStyle
        }
    }
    private func loadUI() {
        self.fd_prefersNavigationBarHidden = true;
        self.magicViewCtrl.magicView.sliderColor = UIColor.appxf1f2f4()
        self.magicViewCtrl.magicView.backgroundColor = UIColor.systemBackground
        self.magicViewCtrl.magicView.separatorColor = UIColor.systemBackground
        self.magicViewCtrl.magicView.navigationColor = UIColor.systemBackground
        self.magicViewCtrl.magicView.backgroundColor = UIColor.systemBackground
        self.addChild(self.magicViewCtrl);
        self.view.addSubview(self.magicViewCtrl.view);
        self.magicViewCtrl.view.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        self.listData.forEach { titleName in
            let vc =  GKClassifyItemController(titleName: titleName)
            self.controllers.append(vc)
        }
        self.magicViewCtrl.magicView.reloadData()
    }
}
extension GKClassifyTabController :VTMagicViewDelegate,VTMagicViewDataSource{
    func menuTitles(for magicView: VTMagicView) -> [String] {
        return self.listTitles;
    }
    func magicView(_ magicView: VTMagicView,menuItemAt: UInt) -> UIButton {
        let button = magicView.dequeueReusableItem(withIdentifier: "com.new.btn.itemIdentifier") ?? UIButton(type: .custom)
        button.setTitleColor(UIColor.appx333333(), for: .normal)
        button.setTitleColor(moya.appColor, for: .selected)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        return button
    }
    func magicView(_ magicView: VTMagicView, viewControllerAtPage pageIndex: UInt) -> UIViewController {
        return self.controllers[Int(pageIndex)]
    }
}
