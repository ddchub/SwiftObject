//
//  GKClassifyItemController.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2018/9/5.
//  Copyright © 2018 wangws1990. All rights reserved.
//
//  
import UIKit

class GKClassifyItemController: BaseConnectionController {
    convenience init(titleName :String) {
        self.init()
        self.titleName = titleName
    }
    private var titleName    :String? = nil
    private lazy var listData:[GKClassifyModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupRefresh(scrollView: self.collectionView, options: [.auto,.header])
    }
    override func vtm_prepareForReuse() {
        self.listData.removeAll();
        self.collectionView.reloadData();
    }
    override func refreshData(page: Int) {
        guard let titleName = self.titleName else { return  self.endRefresh(more: false)}
        ApiMoya.request(target: .classify) { json in
            if page == RefreshPageStart{
                self.listData.removeAll();
            }
            if let list : [JSON] = json[titleName].array{
                for obj in list{
                    guard let user = GKClassifyModel.deserialize(from: obj.rawString()) else { return }
                    self.listData.append(user)
                }
                self.collectionView.reloadData()
                self .endRefresh(more:false)
            }
        } failure: { error in
            self.endRefreshFailure(error: error)
        }
    }
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listData.count
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return GKClassifyCell.cellSize
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell  = GKClassifyCell.cellForCollectionView(collectionView: collectionView, indexPath: indexPath)
        cell.model = self.listData[indexPath.row]
        return cell
    }
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = self.listData[indexPath.row]
        if let title = model.title,let name = self.titleName{
            GKJump.jumpToClassifyTail(group: name, name: title)
        }
    }
}

