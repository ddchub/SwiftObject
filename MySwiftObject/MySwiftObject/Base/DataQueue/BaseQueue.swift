//
//  BaseQueue.swift
//  MySwiftObject
//
//  Created by wangws1990 on 2020/11/25.
//  Copyright © 2020 wangws1990. All rights reserved.
//

import UIKit
import SQLite.Swift

private let id                   = Expression<String>("primaryId")
//表模型数据
private let data                 = Expression<String?>("data")

class BaseQueue{
    private static func table(_ name :String) -> Table?{
        guard let db = dataBase.db else { return nil }
        let table = Table(name)
        let run = table.create(ifNotExists: true, block: { (table) in
            //注意.autoincrement
            table.column(id,primaryKey: true)
            table.column(data)
        })
        guard let _ = try? db.run(run) else { return nil }
        return table
    }
    public static func haveData(_ table :Table,primaryId :String) -> Bool{
        guard let db = dataBase.db else { return false }
        let alice = table.filter(id == primaryId)
        guard let count = try? db.scalar(alice.count) else { return false }
        return count > 0 ? true : false
    }
    public static func insertData(tableName :String,primaryId :String,content : String,completion:@escaping ((_ success : Bool) -> ())){
        guard let db = dataBase.db else { return completion(false) }
        guard let table = table(tableName) else { return completion(false) }
        let have = haveData(table, primaryId: primaryId)
        if !have{
            let insertdata = table.insert(id <- primaryId,data <- content)
            guard let _ = try? db.run(insertdata) else { return completion(false) }
            completion(true)
        }else{
            updateData(tableName: tableName, primaryId: primaryId, content: content, completion: completion)
        }
    }
    public class func updateData(tableName :String,primaryId :String,content : String,completion:@escaping ((_ success : Bool) -> Void)){
        guard let db = dataBase.db else { return completion(false) }
        guard let table = table(tableName) else { return completion(false) }
        let have = haveData(table, primaryId: primaryId)
        if !have {
            insertData(tableName: tableName, primaryId: primaryId, content: content, completion: completion)
        }else{
            let run = table.filter(id == primaryId)
            let update = run.update(id <- primaryId,data <- content)
            guard let _ = try? db.run(update) else { return completion(false) }
            completion(true)
        }
    }
    public class func deleteData(tableName :String,primaryId :String,completion:@escaping ((_ success : Bool) -> ())){
        guard let db = dataBase.db else { return completion(false) }
        guard let table = table(tableName) else { return completion(false) }
        let alice = table.filter(id == primaryId)
        guard let _ = try? db.run(alice.delete()) else { return completion(false) }
        completion(true)
    }
    public class func searchData(tableName :String,primaryId :String,completion:@escaping ((_ datas : JSON) -> ())){
        guard let db = dataBase.db else { return completion(false) }
        guard let table = table(tableName) else { return completion(false) }
        let alice = table.filter(id == primaryId)
        guard let datas : AnySequence<Row> = try? db.prepare(alice) else {
            return completion("")
        }
        decodeData(listData: datas, completion: completion)
    }
    public class func searchData(tableName :String,page : Int,size : Int = 20,completion:@escaping ((_ datas : JSON) ->())){
        guard let db = dataBase.db else { return completion(JSON()) }
        guard let table = table(tableName) else { return completion(JSON()) }
        let order = table.order(id.asc).limit(size, offset: (page - 1) * size)

        guard let datas : AnySequence<Row> = try? db.prepare(order) else {
            return completion(JSON(""))
        }
        decodeData(listData: datas,completion: completion)
    }
    public class func searchData(tableName :String,completion:@escaping ((_ datas : JSON) ->())){
        guard let db = dataBase.db else { return completion(JSON()) }
        guard let table = table(tableName) else { return completion(JSON()) }
        guard let datas : AnySequence<Row> = try? db.prepare(table) else {
            return completion(JSON(""))
        }
        decodeData(listData: datas,completion: completion)
    }
    private class func decodeData(listData : AnySequence<Row>,completion:@escaping ((_ datas : JSON) ->Void)){
        DispatchQueue.global().async {
            var contentData : [JSON] = []
            listData.forEach { (objc) in
                if let json = try? objc.get(data) {
                    let json = JSON(parseJSON:json)
                    contentData.append(json)
                }
            }
            let json = JSON(contentData)
            DispatchQueue.main.async {
                completion(json)
            }
        }
    }
    public class func dropTable(_ name :String,completion:@escaping ((_ success : Bool) ->Void)){
        let exeStr = "drop table if exists \(name) "
        guard let db = dataBase.db else { return completion(false)}
        guard let _ = try? db.execute(exeStr) else { return completion(false) }
    }
}
