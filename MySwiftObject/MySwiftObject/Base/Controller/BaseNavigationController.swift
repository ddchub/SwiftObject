//
//  BaseNavigationController.swift
//  GKGame_Swift
//
//  Created by wangws1990 on 2018/9/29.
//  Copyright © 2018 wangws1990. All rights reserved.
//  
import UIKit
import RxCocoa
class BaseNavigationController: UINavigationController {
    private var pushing : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.interactivePopGestureRecognizer?.isEnabled = true
        self.setNaviBackground()
        self.overrideUserInterfaceStyle = moya.interfaceStyle
    }
    override var prefersStatusBarHidden: Bool{
        guard let visibleViewController = self.visibleViewController else { return false }
        return visibleViewController.prefersStatusBarHidden
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        guard let visibleViewController = self.visibleViewController else { return .default }
        return visibleViewController.preferredStatusBarStyle
    }
    override var shouldAutorotate: Bool{
        guard let visibleViewController = self.visibleViewController else { return false }
        return visibleViewController.shouldAutorotate;
    }
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        guard let visibleViewController = self.visibleViewController else { return .portrait }
        return visibleViewController.supportedInterfaceOrientations
    }
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation{
        guard let visibleViewController = self.visibleViewController else { return .portrait }
        return visibleViewController.preferredInterfaceOrientationForPresentation
    }
}
extension BaseNavigationController :UINavigationControllerDelegate{
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if self.pushing == true {
            return;
        }else{
            self.pushing = true;
        }
        super.pushViewController(viewController, animated: animated);
    }
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        self.pushing = false;
    }
}
