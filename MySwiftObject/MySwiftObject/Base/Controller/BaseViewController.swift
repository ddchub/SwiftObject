//
//  BaseViewController.swift
//  GKGame_Swift
//
//  Created by wangws1990 on 2018/9/29.
//  Copyright © 2018 wangws1990. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    deinit {
        debugPrint(self.classForCoder)
        removeNotification()
    }
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        addNotification()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    public lazy var disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.systemBackground
        self.edgesForExtendedLayout = []
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.overrideUserInterfaceStyle = moya.interfaceStyle
        self.fd_prefersNavigationBarHidden = false
        self.fd_interactivePopDisabled = false
    }
    func loadBaseUI() {
        
    }
    private func addNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(notificationAction(sender:)), name: AppThemeNotification, object: nil)
    }
    private func removeNotification(){
        NotificationCenter.default.removeObserver(self, name: AppThemeNotification, object: nil)
    }
    @objc private func notificationAction(sender :Notification){
        if let selectColor = sender.object as? UIColor {
            self.navigationController?.viewControllers.forEach({ vc in
                debugPrint(vc.classForCoder)
                vc.overrideUserInterfaceStyle = moya.interfaceStyle
                vc.navigationController?.overrideUserInterfaceStyle = moya.interfaceStyle
                if let baseController = vc as? BaseViewController{
                    baseController.loadBaseUI()
                }
                vc.navigationController?.setNaviBackground(selectColor)
                vc.setNeedsStatusBarAppearanceUpdate()
                vc.showNavTitle(title: vc.navigationItem.title)
            })
        }
    }
    override var prefersStatusBarHidden: Bool{
        return false
    }
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return moya.interfaceStyle == .light ? .default : .darkContent
    }
    override var shouldAutorotate: Bool{
        return true
    }
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return .all
    }
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation{
        return .portrait
    }
}
extension BaseViewController : UIGestureRecognizerDelegate{
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
extension UIViewController{
    ///MARK:图片放到导航栏有点不规则因此写个拓展
    public func showNavTitle(title :String?){
        self.showNaviTitle(title: title)
       // self.showNaviTitle(title: title, backIcon: moya.theme.naviBack, closeIcon: moya.theme.naviBack)
      //  self.showNavTitle(title: title, backIcon: moya.theme.naviBack)
    }
}

